import os

import dash_bootstrap_components as dbc
from dash import Dash
import flask

from emissions_dashboard import project_directory
from version import __version__ as version
from config import global_config
import layout


server = flask.Flask("emissions-dashboard")
app = Dash(
    __name__,
    use_pages=True,
    server=server,
    external_stylesheets=[dbc.themes.BOOTSTRAP],
    assets_folder=project_directory / "assets",
)
app.layout = layout.layout
app.title = global_config.application_name


@server.route("/health-check")
def health_check():
    return global_config.application_name + " " + version
