from config import global_config
from app import app, server  # noqa

if __name__ == "__main__":
    app.run_server(debug=global_config.debug)
