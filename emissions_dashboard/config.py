from pathlib import Path

from pydantic import BaseSettings

from emissions_dashboard import project_directory


class GlobalConfig(BaseSettings):
    debug: bool = False
    running_local: bool = False

    environment_slug: str = "testing"
    application_name: str = "Greenhouse gas emissions dashboard"

    path_owid_description: Path = (
        project_directory / "../data/owid/co2-data/owid-co2-codebook.csv"
    )
    path_owid_dataset: Path = (
        project_directory / "../data/owid/co2-data/owid-co2-data.csv"
    )


global_config = GlobalConfig()


GRAPH_CONFIG = {
    "displayModeBar": True,
    "displaylogo": False,
    "modeBarButtonsToRemove": [
        "pan2d",
        "select2d",
        "lasso2d",
        "zoomIn2d",
        "zoomOut2d",
        "autoScale2d",
        "toImage",
    ],
}
