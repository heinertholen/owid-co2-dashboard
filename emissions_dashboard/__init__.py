from pathlib import Path
import os

project_directory = Path(os.path.dirname(os.path.abspath(__file__)))
project_directory = project_directory / ".."
project_directory = project_directory.resolve()
