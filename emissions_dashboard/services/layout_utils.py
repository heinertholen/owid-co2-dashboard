import dash_bootstrap_components as dbc


def col(*children, class_name="m-1", **kwargs):
    return dbc.Col(children, **kwargs, class_name=class_name)


def row(*children, class_name="m-1", **kwargs):
    return dbc.Row(children, **kwargs, class_name=class_name)
