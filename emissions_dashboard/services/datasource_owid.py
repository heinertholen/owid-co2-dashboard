import pandas as pd

from emissions_dashboard.config import global_config

# manual cache
CACHE: dict[str, pd.DataFrame] = {}


def description() -> pd.DataFrame:
    if "description" in CACHE:
        return CACHE["description"]
    df = pd.read_csv(global_config.path_owid_description)
    CACHE["description"] = df
    return df


def dataset() -> pd.DataFrame:
    if "dataset" in CACHE:
        return CACHE["dataset"]
    df = pd.read_csv(global_config.path_owid_dataset)
    CACHE["dataset"] = df
    return df
