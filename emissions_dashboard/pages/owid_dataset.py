from dash import dash_table, html, dcc
import dash

from emissions_dashboard.services import datasource_owid

PAGE_NAME = "OWID Dataset Description"
dash.register_page(__name__, name=PAGE_NAME)


def layout(
    # catch all args so the layout is always loaded
    # (dash puts all args from the URL as keywords into this function)
    **_,
):
    content = html.Div(
        children=[
            html.H2(PAGE_NAME),
            dcc.Markdown(
                """
From "Our world in data":
  * [OWID page on the dataset](https://ourworldindata.org/co2-and-greenhouse-gas-emissions)
  * [Original source of the data](https://github.com/owid/co2-data)            
            """
            ),
            df_to_table(datasource_owid.description()),
        ],
    )
    return content


def df_to_table(df) -> dash_table.DataTable:
    table = dash_table.DataTable(
        data=df.to_dict("records"),
        columns=[{"name": i, "id": i} for i in df.columns],
        style_data={
            "whiteSpace": "normal",
            "height": "auto",
            "lineHeight": "15px",
        },
    )
    return table
