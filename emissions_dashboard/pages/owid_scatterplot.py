from dash import Input, Output, State, dcc, html
from plotly.subplots import make_subplots
import plotly.express as px
import dash

from emissions_dashboard.services.layout_utils import col, row
from emissions_dashboard.services import datasource_owid
from emissions_dashboard.config import GRAPH_CONFIG

PAGE_NAME = "OWID Dataset Scatterplot"
dash.register_page(__name__, name=PAGE_NAME)


def layout(
    # catch all args so the layout is always loaded
    # (dash puts all args from the URL as keywords into this function)
    **_,
):
    df = datasource_owid.dataset()
    min_year = int(df["year"].min())
    max_year = int(df["year"].max())

    return html.Div(
        children=[
            html.H2(PAGE_NAME),
            row(
                col(
                    row(
                        html.Label("x-axis"),
                        dcc.Dropdown(
                            id="dropdown-owid-x-axis",
                            options=df.columns,
                            value="year",
                        ),
                    ),
                    row(
                        html.Label("y-axis"),
                        dcc.Dropdown(
                            id="dropdown-owid-y-axis",
                            options=df.columns,
                            value="co2",
                        ),
                    ),
                    row(
                        html.Label("point color"),
                        dcc.Dropdown(
                            id="dropdown-owid-color",
                            options=df.columns[df.dtypes == object],
                            value="country",
                        ),
                    ),
                    row(
                        html.Label("country allow-list"),
                        dcc.Dropdown(
                            id="dropdown-owid-country-allow",
                            multi=True,
                            options=df["country"].unique(),
                        ),
                    ),
                    row(
                        html.Label("country block-list"),
                        dcc.Dropdown(
                            id="dropdown-owid-country-block",
                            multi=True,
                            options=df["country"].unique(),
                        ),
                    ),
                    row(
                        html.Label("year range"),
                        dcc.RangeSlider(
                            min_year,
                            max_year,
                            5,
                            marks={min_year: str(min_year), max_year: str(max_year)},
                            id="dropdown-owid-year-range",
                        ),
                    ),
                    row(
                        html.Label("opacity"),
                        dcc.Slider(0, 10, 1, value=5, id="slider-owid-opacity"),
                    ),
                    width=3,
                ),
                col(
                    row(
                        dcc.Loading(
                            dcc.Graph(
                                id="owid-graph",
                                figure=apply_figure_style(make_subplots()),
                                config=GRAPH_CONFIG,
                                style={"height": "80vh", "minHeight": "300px"},
                            ),
                        ),
                    ),
                ),
            ),
        ]
    )


@dash.callback(
    Output("owid-graph", "figure"),
    Input("dropdown-owid-x-axis", "value"),
    Input("dropdown-owid-y-axis", "value"),
    Input("dropdown-owid-color", "value"),
    Input("dropdown-owid-country-allow", "value"),
    Input("dropdown-owid-country-block", "value"),
    Input("dropdown-owid-year-range", "value"),
    Input("slider-owid-opacity", "value"),
)
def update_graph(
    x_axis,
    y_axis,
    color_by_category,
    country_allow,
    country_block,
    year_range,
    opacity,
):
    if not (x_axis and y_axis):
        return dash.no_update

    df = get_data(country_allow, country_block, year_range)
    figure = px.scatter(
        df,
        x=x_axis,
        y=y_axis,
        color=color_by_category,
        opacity=opacity / 10,
    )
    apply_figure_style(figure)

    return figure


def get_data(country_allow, country_block, year_range):
    df = datasource_owid.dataset()
    if country_allow:
        df = df.loc[df.country.isin(country_allow)]
    if country_block:
        df = df.loc[~df.country.isin(country_block)]
    if year_range:
        year_min, year_max = year_range
        df = df.loc[(df["year"] >= year_min) & (df["year"] <= year_max)]
    return df


def apply_figure_style(figure):
    figure.layout.template = "plotly_white"
    figure.update_layout(
        margin=dict(l=5, r=5, t=5, b=5),
    )
    return figure
