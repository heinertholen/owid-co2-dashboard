import dash
from dash import html

dash.register_page(__name__, path="/")


def layout():
    div = html.Div(
        children=[
            html.H1("Home"),
            html.P("Choose a page from the menu."),
        ]
    )
    return div
