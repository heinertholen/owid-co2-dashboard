from dash import ClientsideFunction, Input, Output, State, dcc, html
import dash_bootstrap_components as dbc
import dash

from emissions_dashboard.services.layout_utils import col, row
from version import __version__ as version
from config import global_config


def layout(
    # catch all args so the layout is always loaded
    # (dash puts all args from the URL as keywords into this function)
    **_,
):
    menu_entries = [html.P(f"Version {version}")]
    menu_entries += [
        html.Div(
            dcc.Link(
                f"{page['name']}",
                href=page["relative_path"],
            )
        )
        for page in dash.page_registry.values()
    ]

    div = html.Div(
        [
            html.Div(
                children=row(
                    col(dbc.Button("menu", id="button-menu", n_clicks=0)),
                    col(
                        html.H4(global_config.application_name),
                        style={"text-align": "end"},
                    ),
                ),
                style={"background": "#eee"},
            ),
            dbc.Offcanvas(
                menu_entries,
                id="offcanvas-menu",
                title="menu",
                is_open=False,
            ),
            row(col(dash.page_container)),
            dcc.Location(id="location-url", refresh=False),
        ],
    )
    return div


dash.clientside_callback(
    ClientsideFunction(namespace="clientside", function_name="toggle_menu"),
    Output("offcanvas-menu", "is_open"),
    Input("button-menu", "n_clicks"),
    Input("location-url", "pathname"),
    State("offcanvas-menu", "is_open"),
)
