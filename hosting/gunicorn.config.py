"""Gunicorn configuration."""

import multiprocessing

bind = "127.0.0.1:5000"

# number of processes
workers = multiprocessing.cpu_count() + 1
worker_class = "sync"

# this directive logs all accesses to stdout where it can be picked up by the
# Docker daemon
accesslog = "-"

user = "nginx"
