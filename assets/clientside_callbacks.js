window.dash_clientside = Object.assign({}, window.dash_clientside, {
    clientside: {
        toggle_menu:     function(clicks, url, isOpen) {
        let triggererId = dash_clientside.callback_context.triggered.map(t => t["prop_id"]);
        if (triggererId && triggererId[0] === "location-url.pathname") {
            return false;
        }
        if (clicks) {
            return !isOpen;
        }
        return isOpen;
    }
    }
});
