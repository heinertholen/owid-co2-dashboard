FROM python:3.11-slim-bullseye

ENV LANG C.UTF-8
ENV PYTHONPATH $PYTHONPATH:/srv
ENV PATH="/root/.local/bin:${PATH}"
ENV PIP_NO_CACHE_DIR false

WORKDIR /srv

# install get reqs for webserver, and empty the apt cache afterwards
RUN apt-get update -qq && apt-get install -qqy --no-install-recommends \
    git \
    curl \
    nginx \
    supervisor && \
    rm -rf /var/lib/apt/lists/*

RUN pip install --default-timeout=200 --upgrade pip==23.1 && \
    pip install pipenv==2023.4.20

COPY Pipfile Pipfile.lock /srv/

# FUTURE: include secrets from outside
# RUN --mount=type=secret,id=token . /run/secrets/token && \
RUN pipenv install --deploy --system

# the nginx and supervisor setup
RUN useradd --no-create-home nginx

COPY . /srv

# in order to keep the number of layers down, we don't use indiviual COPY
# commands, but rather one RUN command once the files are in the image
RUN mv /srv/hosting/nginx.conf /etc/nginx/ && \
    mv /srv/hosting/flask-site-nginx.conf /etc/nginx/conf.d/

# download data
RUN git clone https://github.com/owid/co2-data --depth 1 /data/owid/co2-data

# for exposing port 80 for webapps
EXPOSE 80

CMD ["/usr/bin/supervisord", "-n", "-c", "/srv/hosting/supervisord.conf"]
